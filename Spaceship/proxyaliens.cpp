#include "proxyaliens.h"

namespace si{

RealAliens ProxyAliens::request(int number)
{
    if(number > 6){
        std::cout << "too many aliens" << std::endl;
        exit(1);
    }
    RealAliens m_realAli (m_t);
    return m_realAli;
}
}
