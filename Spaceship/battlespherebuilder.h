#ifndef BATTLESPHEREBUILDER_H
#define BATTLESPHEREBUILDER_H

#include <string>
#include "realaliens.h"
#include "battlesphere.h"
#include "defender.h"
#include "commandcentre.h"

namespace si {

    class BattlesphereBuilder
    {
    public:
        BattlesphereBuilder(){}

        void buildDefender(const Defender &d);

        void buildBulletParameters(int movementSpeed);

        //build aliens functions
        void buildTypeA(std::vector<RealAliens> m_va);
        void buildTypeB(std::vector<RealAliens> m_vb);
        void buildTypeC(std::vector<RealAliens> m_vc);

        void buildCommandCentre(const CommandCentre& commandCentre);

        si::BattleSphere* getBattlesphere();

    private:
        Defender m_defender;
        int m_bulletSpeed;
        CommandCentre m_commandCentre;

        //vectors containing aliens
        std::vector<RealAliens> m_a;
        std::vector<RealAliens> m_b;
        std::vector<RealAliens> m_c;
    };

} // end namespace si

#endif // BATTLESPHEREBUILDER_H
