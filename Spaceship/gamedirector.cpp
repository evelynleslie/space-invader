#include "gamedirector.h"
#include "realaliens.h"
#include "commandcentre.h"
#include "iofile.h"

namespace si {

    /**
     * \brief: Basic game creation, there may be more of these methods if
     *         more levels are added to the game.
     * \result: builder, the builder helper used to create the game object
     */
    BattleSphere* GameDirector::createBattleSphereStandardMode(BattlesphereBuilder& builder)
    {
        // Read the IO file inside here
        //IOFile ioFile("config.ini");
        IOFile* ioFile = IOFile::getInstance("../Spaceship/config.ini"); //singleton
        CommandCentre commandCentre = ioFile->getCommandCentre(); //an instance of command centre
        Defender defender = ioFile->getDefender(); //an instance of defender

        //gets aliens
        std::vector<RealAliens> m_va = ioFile->m_a;
        std::vector<RealAliens> m_vb = ioFile->m_b;
        std::vector<RealAliens> m_vc = ioFile->m_c;

        builder.buildDefender(defender); //builder builds defender
        builder.buildBulletParameters(40); //builder builds bullet param
        builder.buildCommandCentre(commandCentre); //builder builds commamd centre

        //builds aliens
        builder.buildTypeA(m_va);
        builder.buildTypeB(m_vb);
        builder.buildTypeC(m_vc);

        delete ioFile;
        return builder.getBattlesphere(); //builder gets battlesphere
    }

} // end namespace si
