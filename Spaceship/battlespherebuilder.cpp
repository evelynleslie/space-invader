#include "battlespherebuilder.h"

namespace si {

    /**
     * \brief: Constructs the player itself
     * \param: defender, an instance of a player
     */
    void BattlesphereBuilder::buildDefender(const Defender& d)
    {
        m_defender = d;
    }


    void BattlesphereBuilder::buildTypeA(std::vector<RealAliens> m_va){
        for(int i = 0; i < m_va.size(); i++){
            //a is located at the top
            m_va[i].m_y = 70;
            if(m_va[0].m_trajectory == "left"){
                //initializes the aliens at far right
                m_va[0].m_x = 600;
                m_va[i].m_x = 600 + (50*-i);
            }
            else if(m_va[0].m_trajectory == "right"){
                //initializes the aliens at far left
                m_va[i].m_x = 50*i;
            }
            else{
                //initializes the aliens at the middle
                m_va[0].m_x = 300;
                m_va[i].m_x = 300 + (50*i);
            }
        }
        m_a = m_va;
    }

    void BattlesphereBuilder::buildTypeB(std::vector<RealAliens> m_vb){
        for(int i = 0; i < m_vb.size(); i++){
            //b is at the middle
            m_vb[i].m_y = 130;
            if(m_vb[0].m_trajectory == "left"){
                m_vb[0].m_x = 600;
                m_vb[i].m_x = 600 + (50*-i);
            }
            else if(m_vb[0].m_trajectory == "right"){
                m_vb[i].m_x = 50*i;
            }
            else{
                m_vb[0].m_x = 300;
                m_vb[i].m_x = 300 + (50*i);
            }
        }
        m_b = m_vb;
    }

    void BattlesphereBuilder::buildTypeC(std::vector<RealAliens> m_vc){
        for(int i = 0; i < m_vc.size(); i++){
            //c is located at the bottom
            m_vc[i].m_y = 190;
            if(m_vc[0].m_trajectory == "left"){
                m_vc[0].m_x = 600;
                m_vc[i].m_x = 600 + (50*-i);
            }
            else if(m_vc[0].m_trajectory == "right"){
                m_vc[i].m_x = 50*i;
            }
            else{
                m_vc[0].m_x = 300;
                m_vc[i].m_x = 300 + (50*i);
            }
        }
        m_c = m_vc;
    }

    /**
     * \brief: Sets number of pixels bullet should move in one frame
     * \param: movementSpeed, number of pixels for bullet to travel in one frame
     */
    void BattlesphereBuilder::buildBulletParameters(int movementSpeed)
    {
        m_bulletSpeed = movementSpeed;
    }

    /**
     * \brief: Builds the user interface buffer (an IOFile in this case
     *         but could be used to store events entered on keyboard).
     * \param: commandCentre
     */
    void BattlesphereBuilder::buildCommandCentre(const CommandCentre& commandCentre)
    {
        m_commandCentre = commandCentre;
    }

    /**
     * \brief: Returns a game instance from our builder
     * \result: an instance of the BattleSphere game
     */
    BattleSphere* BattlesphereBuilder::getBattlesphere()
    {
        si::BattleSphere* battlesphere = new
                si::BattleSphere(nullptr, m_defender, m_bulletSpeed, m_commandCentre, m_a, m_b, m_c);
        return battlesphere;
    }

} // end namespace si

