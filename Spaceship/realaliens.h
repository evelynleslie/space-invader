#ifndef REALALIENS_H
#define REALALIENS_H

#include <string>
#include <iostream>

namespace si{

class RealAliens
{
public:
    int m_x = 0;
    int m_y = 0;
    std::string m_trajectory;
    std::string m_t;
    RealAliens(std::string type):
        m_t(type){}

};

}

#endif // REALALIENS_H
