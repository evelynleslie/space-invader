#include "beam.h"

namespace si {

    void Beam::updateX(int updateAmount)
    {
        m_b.updateX(updateAmount);
    }


    void Beam::updateY(int updateAmount)
    {
       m_b.updateY(updateAmount);
    }

    int Beam::getX(){
        return m_b.getX();
    }

    int Beam::getY(){
        return m_b.getY();
    }
} // end namespace si

