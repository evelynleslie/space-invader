#ifndef ALIENS_H
#define ALIENS_H

#include <string>
#include "realaliens.h"

namespace si {
    class Aliens
    {
    public:
        virtual RealAliens request(int number) = 0;
    };
} // end namespace si

#endif // ALIENS_H
