#include "battlesphere.h"

#include <QLabel>
#include <QHBoxLayout>
#include <random>

namespace si {

    /**
     * \brief: Constructs BattleSphere game using the provided defender
     * \param: parent, not used here
     * \param: d, defender describing where the player position is initially
     * \param: bulletSpeed, how many pixels to move bullet in one frame
     * \param: commandCentre, stores buffer of user inputs (just file inputs initially)
     * \result: the person
     */
    BattleSphere::BattleSphere(QWidget *parent,
                           Defender d, int bulletSpeed, CommandCentre cc,
                           std::vector<RealAliens> a, std::vector<RealAliens> b, std::vector<RealAliens> c)
        : QDialog(parent),
          m_defender(d),
          m_bulletSpeed(bulletSpeed),
          m_commandCentre(cc),
          m_a(a),
          m_b(b),
          m_c(c)
    {
        //displays counter
        m_label = new QLabel(this);
        m_layout = new QHBoxLayout();

        m_label->setText("Score: ");
        m_label->setStyleSheet("background-color: #FFFFFF;");
        m_layout->addWidget(m_label);
        m_layout->setGeometry(QRect(QPoint(301, 0),QSize(299, 50)));

        for (int i=0; i<m_numStars; ++i) {
            int randX = rand() % m_screenWidth;
            int randY = rand() % m_screenHeight;

            float randStartOpacity = ((double) rand() / (RAND_MAX)) + 1;
            Star curStar(randX, randY);
            curStar.setOpacity(randStartOpacity);
            m_stars.push_back(curStar);
        }

        //aliens' speed
        m_da = 10;
        m_db = 10;
        m_dc = 10;

        m_defenderImg.load(":/images/spaceship.png");
        m_bulletImg.load(":/images/laser.png");
        m_starImg.load(":/images/star.png");
        m_aImg.load(":images/a.png");
        m_bImg.load(":images/typebb.png");
        m_cImg.load(":images/c.png");

        m_beamA.load(":/images/beamA.png");
        m_beamBL.load(":/images/beamBLeft.png");
        m_beamR.load(":/images/beamBRight.png");
        m_beamC.load(":/images/beamCD.png");

        m_starImg = m_starImg.scaledToWidth(5);
        m_aImg = m_aImg.scaledToWidth(50);
        m_bImg = m_bImg.scaledToWidth(50);
        m_cImg = m_cImg.scaledToWidth(50);

        m_beamA = m_beamA.scaledToWidth(20);
        m_beamBL = m_beamBL.scaledToWidth(20);
        m_beamR = m_beamR.scaledToWidth(20);
        m_beamC = m_beamC.scaledToWidth(20);

        //transform image on y:axis
        if(m_a.size() > 0){
            if(m_a[0].m_trajectory == "right"){
                transformImg("a");
            }

        }
        if(m_c.size() > 0){
            if(m_c[0].m_trajectory == "right"){
                transformImg("c");
            }
        }

        if (d.getScale() == "tiny") {
            m_defenderImg = m_defenderImg.scaledToWidth(80);
        } else if (d.getScale() == "normal") {
            m_defenderImg = m_defenderImg.scaledToWidth(100);
        } else if (d.getScale() == "large") {
            m_defenderImg = m_defenderImg.scaledToWidth(120);
        } else if (d.getScale() == "giant") {
            m_defenderImg = m_defenderImg.scaledToWidth(140);
        }

        setStyleSheet("background-color: #000000;");
        this->resize(m_screenWidth, m_screenHeight);
        update();

        // Create the button, make "this" the parent
        m_button = new QPushButton("Save Screenshot!", this);
        // set size and location of the button
        m_button->setGeometry(QRect(QPoint(0, 0),
        QSize(300, 50)));
        m_button->setStyleSheet("background-color: #FFFFFF;");

        m_timer = new QTimer(this);
        connect(m_timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
        connect(m_button, SIGNAL(released()), this, SLOT(screenshot()));
        m_timer->start(50);
    }

    /**
     * \brief: Destroys dynamically allocated variables
     */
    BattleSphere::~BattleSphere() {
        delete m_timer;
        delete m_button;
    }

    /**
     * \brief: called on each frame, re draws all objects
     * \param: event, description of when to perform the redraw
     */
    void BattleSphere::paintEvent(QPaintEvent *event) {
        QPainter painter(this);

        painter.drawPixmap(m_defender.getX(), m_defender.getY(), m_defenderImg);

        //paint aliens
        for(int i = 0; i < m_c.size(); i++){
            //shoots beam once for every iteration
            //iteration as in: one full left and right
            if(m_c[0].m_x == 0 || m_c[0].m_x == 600){
                hasShotC = false;
            }
            if(m_c[0].m_x != 0 && m_c[0].m_x != 600){
                //generates number random in a range
                int output = (std::rand() % (int)(m_c.size()));
                shoot("c",output);
            }
            painter.drawPixmap(m_c[i].m_x, m_c[i].m_y, m_cImg);
            shot(i,"c");
        }

        for(int i = 0; i < m_b.size(); i++){
            if(m_b[0].m_x == 0 || m_b[0].m_x == 600){
                hasShotB = false;
            }
            if(m_b[0].m_x != 0 && m_b[0].m_x != 600){
                int output = (std::rand() % (int)(m_b.size()));
                shoot("b",output);
            }
            painter.drawPixmap(m_b[i].m_x, m_b[i].m_y, m_bImg);
            shot(i,"b");
        }

        for(int i = 0; i < m_a.size(); i++){
            if(m_a[0].m_x == 0 || m_a[0].m_x == 600){
                hasShotA = false;
            }
            //alien a only shoots if a defender is detected
            if(m_defender.getX() + m_defenderImg.width()/2 > m_a[i].m_x && m_defender.getX() + m_defenderImg.width()/2 < m_a[i].m_x + m_aImg.width()){
                if(m_a[0].m_x != 0 && m_a[0].m_x != 600){
                    shoot("a",i);
                }
            }
            painter.drawPixmap(m_a[i].m_x, m_a[i].m_y, m_aImg);
            shot(i,"a");
        }

        for (auto &curBullet : m_bullets) {
            painter.drawPixmap(curBullet.getX(), curBullet.getY(), m_bulletImg);
            curBullet.updateY(m_bulletSpeed);
        }

        for(int i = 0; i < m_beams.size(); i++){
            m_beams[i].updateY(-10);
            if(m_beams[i].m_t == "a"){
                painter.drawPixmap(m_beams[i].getX(), m_beams[i].getY(), m_beamA);
            }
            else if(m_beams[i].m_t == "c"){
                painter.drawPixmap(m_beams[i].getX(), m_beams[i].getY(), m_beamC);
            }
            else{
                //alien b shoots beams at angles
                if(i%2==0){
                    //shoots to the right
                    m_beams[i].updateX(10);
                    painter.drawPixmap(m_beams[i].getX(), m_beams[i].getY(), m_beamR);
                }
                else{
                    //shoots to the left
                    m_beams[i].updateX(-10);
                    painter.drawPixmap(m_beams[i].getX(), m_beams[i].getY(), m_beamBL);
                }
            }
            if(m_beams[i].getX() + m_bulletImg.width()/2 > m_defender.getX() && m_beams[i].getX() + m_bulletImg.width()/2 < m_defender.getX() + m_defenderImg.width()){
                if(m_beams[i].getY() + m_bulletImg.height() > m_defender.getY() && m_beams[i].getY() < m_defender.getY()){
                    exit(0);
                }
            }
        }

        for (auto &curStar : m_stars) {
            if (curStar.getOpacity() > 0.6 && curStar.getOpacityDelta() > 0) {
                curStar.toggleOpacityDelta();
            } else if (curStar.getOpacity() < 0.1 && curStar.getOpacityDelta() < 0) {
                curStar.toggleOpacityDelta();
            }
            curStar.setOpacity(curStar.getOpacity() + curStar.getOpacityDelta());
            painter.setOpacity(curStar.getOpacity());
            painter.drawPixmap(curStar.getX(), curStar.getY(), m_starImg);
        }
    }

    /**
     * \brief: runs every X seconds, where X is the value sent to the timer
     *         function in the input
     */
    void BattleSphere::nextFrame() {

        for(int i = 0; i < m_a.size(); i++){
            int tmp = m_da;
            //gets next xcooor
             m_a[i].m_x = xcoor(m_a[i].m_x,m_a[i].m_t);
             //if a edge is detected, update all value to be the opposite way
             if(tmp != m_da && i == m_a.size()-1){
                 m_da *= 2;
                 for(int j = 0; j < m_a.size()-1; j++){
                     m_a[j].m_x = xcoor(m_a[j].m_x,m_a[j].m_t);
                 }
                 m_da = m_da/2;
             }
        }

        for(int i = 0; i < m_b.size(); i++){
            int tmp = m_db;
             m_b[i].m_x = xcoor(m_b[i].m_x,m_b[i].m_t);
             if(tmp != m_db && i == m_b.size()-1){
                 m_db *= 2;
                 for(int j = 0; j < m_b.size()-1; j++){
                     m_b[j].m_x = xcoor(m_b[j].m_x,m_b[j].m_t);
                 }
                 m_db = m_db/2;
             }
        }

        for(int i = 0; i < m_c.size(); i++){
            int tmp = m_dc;
             m_c[i].m_x = xcoor(m_c[i].m_x,m_c[i].m_t);
             if(tmp != m_dc && i == m_c.size()-1){
                 m_dc *= 2;
                 for(int j = 0; j < m_c.size()-1; j++){
                     m_c[j].m_x = xcoor(m_c[j].m_x,m_c[j].m_t);
                 }
                 m_dc = m_dc/2;
             }
        }

        // animate the defender
        int maxX = this->width() - m_defenderImg.width();

        if (m_commandCentre.hasNext()) {
            std::string nextCommand = m_commandCentre.popNext();
            if (nextCommand == "Left") {
                m_defender.setX(m_defender.getX() - m_defender.getSpeed());
                if (m_defender.getX() < 0) {
                    m_defender.setX(0);
                }
            } else if (nextCommand == "Right") {
                m_defender.setX(m_defender.getX() + m_defender.getSpeed());
                if (m_defender.getX() > maxX) {
                    m_defender.setX(maxX);
                }
            } else if (nextCommand == "Fire") {
                int bx = m_defender.getX() + (m_defenderImg.width()/2) - (m_bulletImg.width()/2);
                int by = m_defender.getY() - m_bulletImg.height();
                Bullet b(bx, by);
                m_bullets.push_back(b);
            }
        }
        update();
    }

    void BattleSphere::screenshot()
    {
        QWidget *w = QApplication::activeWindow();
        if(w) {
            //QPixmap p = QPixmap::grabWidget(w);
            //^ QPixmap::grabWidget is deprecated, use QWidget::grab() instead

            QImage p = w->grab().toImage();
            p.save(QString("BattleSphereScreenshot.png"));
        }
    }

    //shoots beams
    void BattleSphere::shoot(std::string type, int i){
        if(type == "a"){
            if(hasShotA == false){
                int y = m_a[i].m_y + m_aImg.height();
                Beam tmp (m_a[i].m_x + m_aImg.width()/2,y,"a");
                m_beams.push_back(tmp);
                alienSound("pew");
                hasShotA = true;
            }
        }
        else if(type == "b"){
            if(hasShotB == false){
                int y = m_b[i].m_y + m_bImg.height();
                Beam tmp (m_b[i].m_x + m_bImg.width()/2,y,"b");
                m_beams.push_back(tmp);
                alienSound("pew");
                hasShotB = true;
            }
        }
        else{
            if(hasShotC == false){
                int y = m_c[i].m_y + m_cImg.height();
                Beam tmp (m_c[i].m_x + m_cImg.width()/2,y,"c");
                m_beams.push_back(tmp);
                alienSound("pew");
                hasShotC = true;
            }
        }
    }

    //checks if an alien is shot
    void BattleSphere::shot(int i, std::string type){
        for(int j = 0; j < m_bullets.size(); j++){
            if(type == "a"){
                if(m_bullets[j].getX() + m_bulletImg.width()/2 < m_a[i].m_x + m_aImg.width() && m_bullets[j].getX() + m_bulletImg.width()/2 > m_a[i].m_x){
                    if(m_bullets[j].getY() < m_a[i].m_y + m_aImg.height() && m_bullets[j].getY() > m_a[i].m_y){
                        //remove alien and bullet if shot
                        m_a.erase(m_a.begin() + i);
                        m_bullets.erase(m_bullets.begin() + j);
                        alienSound("rip");
                        //increase counter
                        n_counter++;
                        //display new value
                        m_label->setText("Score: " + QString::number(n_counter));
                    }
                }
            }
            else if(type == "b"){
                if(m_bullets[j].getX() + m_bulletImg.width()/2 < m_b[i].m_x + m_bImg.width() && m_bullets[j].getX() + m_bulletImg.width()/2 > m_b[i].m_x){
                    if(m_bullets[j].getY() < m_b[i].m_y + m_bImg.height() && m_bullets[j].getY() > m_b[i].m_y){
                        m_b.erase(m_b.begin() + i);
                        m_bullets.erase(m_bullets.begin() + j);
                        alienSound("rip");
                        n_counter++;
                        m_label->setText("Score: " + QString::number(n_counter));
                    }
                }
            }
            else{
                if(m_bullets[j].getX() + m_bulletImg.width()/2 < m_c[i].m_x + m_cImg.width() && m_bullets[j].getX() + m_bulletImg.width()/2 > m_c[i].m_x){
                    if(m_bullets[j].getY() < m_c[i].m_y + m_cImg.height() && m_bullets[j].getY() > m_c[i].m_y){
                        m_c.erase(m_c.begin() + i);
                        m_bullets.erase(m_bullets.begin() + j);
                        alienSound("rip");
                        n_counter++;
                        m_label->setText("Score: " + QString::number(n_counter));
                    }
                }
            }
        }
    }

    void BattleSphere::transformImg(std::string type){
        QTransform transform;
        if(type == "a"){
            QPixmap tmp = m_aImg.transformed(transform.rotate( 180,Qt::YAxis ), Qt::FastTransformation);
            m_aImg = tmp;
        }
        else{
            QPixmap tmp = m_cImg.transformed(transform.rotate( 180,Qt::YAxis ), Qt::FastTransformation);
            m_cImg = tmp;
        }
    }

    int BattleSphere::xcoor(int x, std::string type){

        //if an edge to the right is detected
        //reverse position
        if(x == 600){
           if(type == "a"){
                transformImg("a");
                m_da = -10;
            }
            else if(type == "b"){
                m_db = -10;
            }
           else{
               transformImg("c");
               m_dc = -10;
           }

        }
        //if an edge to the left is detected
        //reverse position
        if(x == 0)
        {
            if(type == "a"){
                transformImg("a");
                m_da = 10;
            }
            else if (type == "b"){
                m_db = 10;
            }
            else{
                transformImg("c");
                m_dc = 10;
            }
        }

        if(type == "a"){
            x += m_da;
        }
        else if (type == "b"){
            x += m_db;
        }
        else{
            x += m_dc;
        }

        return x;
    }

    void BattleSphere::alienSound(std::string fx){

        QString s;

        if(fx == "rip"){
            s = "qrc:/sounds/afx.wav";
        }
        else{
            s = "qrc:/sounds/bubblepop.wav";
        }

        QMediaPlayer * bfx = new QMediaPlayer();
        bfx->setMedia(QUrl(s));

        //if sound fx is currently playing -> restart
        if (bfx->state() == QMediaPlayer::PlayingState){
            bfx->setPosition(0);
        }

        //else if it has stopped -> play it again
        else if (bfx->state() == QMediaPlayer::StoppedState){
             bfx->play();
        }
    }

} // end namespace si
