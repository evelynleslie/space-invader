#ifndef BATTLESPHERE_H
#define BATTLESPHERE_H

#include <QDialog>
#include <QPainter>
#include <QPixmap>
#include <QTimer>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QMediaPlayer>

#include <vector>

#include "beam.h"
#include "commandcentre.h"
#include "defender.h"
#include "iofile.h"
#include "bullet.h"
#include "star.h"

namespace si {

    class BattleSphere : public QDialog
    {
    public:
        BattleSphere(QWidget *parent, Defender d, int bulletSpeed, CommandCentre cc, std::vector<RealAliens> a, std::vector<RealAliens> b, std::vector<RealAliens> c);
        ~BattleSphere();

    private:
        Q_OBJECT
        //images
        QPixmap m_defenderImg;
        QPixmap m_aImg;
        QPixmap m_bImg;
        QPixmap m_cImg;
        QPixmap m_beamA;
        QPixmap m_beamBL;
        QPixmap m_beamR;
        QPixmap m_beamC;
        QPixmap m_bulletImg;
        QPixmap m_starImg;

        QTimer* m_timer;
        QPushButton *m_button;
        QLabel *m_label;
        QHBoxLayout *m_layout;

        //vectors of objects
        std::vector<Bullet> m_bullets;
        std::vector<RealAliens> m_a;
        std::vector<RealAliens> m_b;
        std::vector<RealAliens> m_c;
        std::vector<Beam> m_beams;
        std::vector<Star> m_stars;

        //variables
        int m_numStars = 30;
        int n_counter = 0;
        int m_bulletSpeed;
        int m_da;
        int m_db;
        int m_dc;
        int m_screenWidth = 600;
        int m_screenHeight = 400;

        bool hasShotA = false;
        bool hasShotB = false;
        bool hasShotC = false;

        CommandCentre m_commandCentre;
        Defender m_defender;

        //functions
        int xcoor(int x, std::string trajectory);
        void transformImg(std::string type);
        void shot(int i, std::string type);
        void shoot(std::string type, int i);
        void alienSound(std::string fx);
    protected:
        void paintEvent(QPaintEvent *event);

    public slots:
        void nextFrame();
        void screenshot();
    };

} // end namespace si

#endif // BATTLESPHERE_H
