#ifndef BEAM_H
#define BEAM_H

#include "bullet.h"
#include <string>

namespace si {

//beam class uses object adapter design pattern
//object: bullet

    class Beam
    {
    public:
        Bullet m_b;
        std::string m_t;
        Beam(int beamStartX, int beamStartY, std::string type):
            m_b(beamStartX, beamStartY),
            m_t(type){}

        void updateX(int updateAmount);
        void updateY(int updateAmount);
        int getX();
        int getY();
    };

} // end namespace si


#endif // BEAM_H
