#ifndef PROXYALIENS_H
#define PROXYALIENS_H

#include "realaliens.h"
#include <string>
#include <iostream>

namespace si{

//proxy design pattern

class ProxyAliens
{
public:

    std::string m_t;
    ProxyAliens(std::string type):
        m_t(type){}
    RealAliens request(int number);

};

}

#endif // PROXYALIENS_H
