#pragma once

#include "config.h"
#include "menu.h"
#include "keys.h"
#include "configfile.h"
#include "ship.h"
#include "swarm.h"
#include "swarminfo.h"
#include <QDialog>
#include <QSoundEffect>
#include <QWidget>
#include <vector>
#include <QMap>
#include "strategy.h"
#include "dialog.h"
#include "mouse.h"

namespace game {
class GameDialog : public QDialog {
    Q_OBJECT

public:
    GameDialog(QWidget* parent = nullptr);
    void generateAliens(const QList<SwarmInfo>& swarms);
    virtual ~GameDialog();
    void changeName(QString new_name);
    QString getName();
    bool proceed(string dieOrNot); //proceed to next level or restart
    int getGameScore();
    void setGameScore(int score); //set gamescore to continue the prev level's score

protected:
    QTimer* timer;
    void paintEvent(QPaintEvent* event);
    void paintBullets(QPainter& painter);
    void updateBullets();
    void paintSwarm(QPainter& painter, AlienBase*& root);
    void checkSwarmCollisions(AlienBase*& root);
    // ship and swarms
    Ship* ship;
    std::vector<Bullet*> bullets;
    AlienBase* swarms;  // swarms is the ROOT node of the composite
    QSoundEffect shipFiringSound;

    // keys
    void mouseReleaseEvent(QMouseEvent*event);
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent *event);
    void timerEvent(QTimerEvent *);
    QMap <int, bool> m_keys; //stores keys and whether or not they're currently pressed
    QTimer* m_shootTimer;

    // about the canvas
    int frames;
    const int WIDTH = 800;
    const int HEIGHT = 600;
    int SCALEDWIDTH;
    int SCALEDHEIGHT;

    QString m_name;
    bool m_aliensDied = false; //true if all aliens have died
    int gameScore;
    int m_counter = 0; //number of dead aliens
    bool m_died = false; //true if the defendant is dead
    Config* c;

    // collision...
    int get_collided_swarm(Bullet*& b, AlienBase*& root);
    int get_collided(Bullet*& b, AlienBase*& root);
    void addBullets(const QList<Bullet*>& list);

    // pausing & menu
    bool paused;
    void pauseStart();
    Menu* menu;

    int m_speed = 50;
    QLabel* m_label;
    int m_level = 1;
    int m_shootSpeed = 100;
    int m_lives = 3;
    bool m_reduceLife = false; //decrements lives
    QList<QPixmap> m_hearts;
    QLabel* m_lbl;

    //controls
    QPushButton* m_mouse;
    QPushButton* m_key;
    QPushButton* m_cfile;
    QString m_control = "cfile";
    Mouse m;
    Keys k;
    ConfigFile m_c;

    // score
public slots:
    void nextFrame();
    // menus
    void showScore();
    void mouseControl();
    void keysControl();
    void configControl();
    void increaseDifficulty(int i);
};
}
