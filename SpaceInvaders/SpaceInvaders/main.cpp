#include "gamedialog.h"
#include "dialog.h"
#include <QApplication>
using namespace game;

int main(int argc, char* argv[]) {
    QApplication a(argc, argv);
    Dialog d;
    int m_scoreAllLevels = 0;
    bool m_startAgain = true;
    int m_levels = 0;

    //while loop for restart
    while(d.getPressed("restart") == true){
        d.setPressed(false, "restart");

        //while loop for levels
        while(m_startAgain == true){
            m_startAgain = false;
            GameDialog w;
            w.increaseDifficulty(m_levels);
            w.setGameScore(m_scoreAllLevels); //updates so that the score = prev level's

            //if null -> sets to config file's name
            if(d.getName().isNull() == true){
                d.addName(w.getName());
                d.setName(w.getName());
            }

            //if not -> sets to input's name
            w.changeName(d.getName());
            w.show();

            if(w.exec()){
            }

            m_startAgain = w.proceed("next level");

            if(m_startAgain == true){
                //updates difficulty for the next level
                m_levels += 10;
                m_scoreAllLevels = w.getGameScore();
                w.accept();
            }

            //if the defendat died but chose to restart
            //sets all progress to 0
            if(w.proceed("died") == true){
                m_scoreAllLevels = w.getGameScore();
                d.addScore(m_scoreAllLevels);
                m_scoreAllLevels = 0;
                m_levels = 0;
                d.show();
                m_startAgain = true;

                if(d.exec()){
                }

                if(d.getPressed("exit") == true){
                    w.accept();
                    d.accept();
                    exit(0);
                }
            }
        }
    }   
    return a.exec();
}
