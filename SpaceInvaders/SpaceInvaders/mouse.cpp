#include "mouse.h"

int Mouse::move(){
    return m_i;
}

void Mouse::setX(int x){
    m_i = x;
}

void Mouse::setEvent(QMouseEvent *event){
    m_e = event;
}

bool Mouse::shoot(){
    //shoots if the left mouse button is pressed
    if(m_e->button() == Qt::LeftButton){
        return true;
    }
    else{
        return false;
    }
}

