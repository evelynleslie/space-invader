#ifndef STRATEGY_H
#define STRATEGY_H

#include <QMap>
#include <string>
#include <iostream>

using namespace std;
//strategy design pattern
class Strategy
{
public:
    Strategy(int i = 0)
        :m_i(i){}
    int m_i;
private:
    //the algorithms to be overwrite/implemented by the control subclasses
    virtual int move();
    virtual bool shoot();
};


#endif // STRATEGY_H
