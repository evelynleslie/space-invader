#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <iostream>
#include <QString>

using namespace std;

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    bool getPressed(string whichBool); //returns the button pressed
    void setPressed(bool b, string whichBool);
    QString getName();
    void setName(QString name); //sets the name to be added
    void addName(QString name); //adds name to scoreboard
    void addScore(int score); //adds score to scoreboard

private slots:
    void on_pushButton_clicked(); //restart button
    void on_pushButton_2_clicked(); //exit button

private:
    Ui::Dialog *ui;
    QString m_name;
    bool m_restart = true;
    bool m_exit = false;
    QList<QString> m_names;
    vector<int> m_score;
};

#endif // DIALOG_H
