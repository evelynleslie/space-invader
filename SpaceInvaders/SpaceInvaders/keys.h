#ifndef KEYS_H
#define KEYS_H
#include "strategy.h"
#include <QMap>
#include <QTimer>

//subclass of the strategy design pattern
//algorithm for arrow keys and space control
class Keys : public Strategy
{
public:
    Keys(int control = 0)
        :Strategy(control){}
    int move();
    bool shoot();
    //sets the key currently pressed
    void setCommands(QMap <int, bool> c);
    void addCommands (bool pressed);
    void setKey(int i);
private:
    QMap <int, bool> m_commands;
};

#endif // KEYS_H
