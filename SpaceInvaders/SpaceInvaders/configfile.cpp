#include "configfile.h"

int ConfigFile::move(){
    if(m_instruction == "Left"){
        //moves left
        return 0;
    }
    else if(m_instruction == "Right"){
        //moves right
        return 1;
    }
    else{
        return -1;
    }
}

bool ConfigFile::shoot(){
    if(m_instruction == "Shoot"){
        return true;
    }
    return false;
}

void ConfigFile::setInstruct(QString ins){
    m_instruction = ins;
}

//index of the current line for reading instructions
int ConfigFile::getIndex(){
    return m_i;
}

void ConfigFile::setIndex(int i){
    m_i = i;
}
