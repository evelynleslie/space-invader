#ifndef MOUSE_H
#define MOUSE_H
#include "strategy.h"
#include <string>
#include <iostream>
#include <QMouseEvent>

//subclass of the strategy design pattern
//algorithm for mouse control
class Mouse : public Strategy
{
public:
    Mouse(int x = 0)
        :Strategy(x){}
    virtual int move();
    void setX(int x); //sets the current defendant's position = mouse's
    void setEvent(QMouseEvent * event);
    virtual bool shoot();
private:
    QMouseEvent * m_e;
};

#endif // MOUSE_H
