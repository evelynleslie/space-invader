#include "dialog.h"
#include "ui_dialog.h"
#include <iostream>
#include <QInputDialog>

using namespace std;

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    //gets name from user for restart
    m_name = QInputDialog::getText(this,"menu","name?");
    m_names.append(m_name);
    setPressed(true, "restart");
}

QString Dialog::getName(){
    return m_name;
}

void Dialog::setName(QString name){
    m_name = name;
}

void Dialog::addScore(int score){
    m_score.push_back(score);
}

void Dialog::addName(QString name){
    m_names.append(name);
}

void Dialog::on_pushButton_2_clicked()
{
    QString tmp;
    for(int i = 0; i < m_names.size(); i++){
        tmp = tmp + m_names.at(i);
        tmp = tmp + ": " +QString::number(m_score.at(i)) + "\n";
    }
    //displays scoreboard
    ui->label->setAlignment(Qt::AlignCenter);
    ui->label->setText(tmp);
    setPressed(true, "exit");
}

bool Dialog::getPressed(string whichBool){
    if(whichBool == "restart"){
        return m_restart;
    }
    else{
        return m_exit;
    }
}

void Dialog::setPressed(bool b, string whichBool){
    if(whichBool == "restart"){
        m_restart = b;
    }
    else{
        m_exit = b;
    }
}
