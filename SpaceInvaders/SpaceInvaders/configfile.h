#ifndef CONFIGFILE_H
#define CONFIGFILE_H
#include "strategy.h"

//subclass of the strategy design pattern
//algorithm for config file control
class ConfigFile : public Strategy
{
public:
    ConfigFile(int index = 0)
        :Strategy(index){}
    int move();
    bool shoot();
    void setInstruct(QString ins); //gets a line of instruction
    int getIndex(); //gets current line
    void setIndex(int i); //sets current line
private:
    QString m_instruction;
};

#endif // CONFIGFILE_H
