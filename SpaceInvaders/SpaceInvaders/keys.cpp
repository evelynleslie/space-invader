#include "keys.h"

int Keys::move(){

    if(m_commands[Qt::Key_Left]){
        return 0;
    }

    if(m_commands[Qt::Key_Right]){
        return 1;
    }

    else{
        return -1;
    }
}

bool Keys::shoot(){
    if(m_commands[Qt::Key_Space]){
        return true;
    }
    return false;
}

void Keys::setCommands(QMap<int, bool> c){
    m_commands = c;
}

void Keys::addCommands(bool pressed){
    //sets to the key to true if it's pressed
    //updates it to false if it's released
    m_commands[m_i] = pressed;
}

void Keys::setKey(int i){
    m_i = i;
}

